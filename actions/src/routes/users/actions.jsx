import { userService } from "../../services/user";

export const USERS_LOADED = 'USERS_LOADED'
export const LOADING_USERS = 'LOADING_USERS'

export function loadingUsers(isLoading){
    return {
        type: 'LOADING_USERS',
        isLoading
    };
}

export function usersLoaded(users) {
    return {
        type: USERS_LOADED,
        users
    }
}

export function loadUsers() {
    return (dispatch)=>{
        dispatch(loadingUsers(true));
        userService.all()
        .then((data)=>{
            console.log(data)
            dispatch(usersLoaded(data));
        })
    }
}
