import React from 'react';
import PropTypes from "prop-types";
import { HashRouter, Link, Switch, Route, withRouter } from 'react-router-dom';
import { compose } from 'recompose';
import { User } from "../user";
import { connect } from 'react-redux'
import { loadUsers } from "./actions";

class UsersSketch extends React.Component {

    static propTypes = {
        match: PropTypes.object.isRequired,
        location: PropTypes.object.isRequired,
        history: PropTypes.object.isRequired,
        loadUsers: PropTypes.func,
        users: PropTypes.array
    };

    state = {
        user: null
    };

    componentDidMount() {
        this.props.loadUsers()
    }

    render() {
        let users = this.props.users;
        return (
            <div>
                <p>
                    <span>Users</span>
                </p>
                <ul>
                    {users && Object.entries(users).map(([key, {name}]) => (
                        <li key={key}>
                            <Link to={`${this.props.match.path}/${key}`}>{name}</Link>
                        </li>
                    ))}
                </ul>
                {this.props.loading ?
                <span>cargando</span>
                :
                <HashRouter>
                    <Switch>
                        <Route path={`${this.props.match.path}`} strict={true} exact={true}>
                            <span>Seleccione un item para editar</span>
                        </Route>
                        <Route path={`${this.props.match.path}/:userId`} component={User}/>
                    </Switch>
                </HashRouter>
                }
            </div>
        )
    }
}

const RoutedUsers = compose(
    withRouter,
)(UsersSketch);

const mapStateToProps = (state) => {
    return state.users;
};

const  mapDispatchToProps = (dispatch) => {
    return {
        loadUsers(){
            dispatch(loadUsers())
        }
    };
};

const Users = connect(mapStateToProps, mapDispatchToProps)(RoutedUsers);

export { Users } ;