import React from 'react';
import { compose } from "recompose";
import { withRouter } from 'react-router-dom';
import PropTypes from "prop-types";
import { userService } from "../../services/user";
import { userChange } from "./actions";
import { connect } from 'react-redux'

class UserSketch extends React.Component {

    static propTypes = {
        match: PropTypes.object.isRequired,
        location: PropTypes.object.isRequired,
        history: PropTypes.object.isRequired
    };

    unlisten(){}

    state = {}

    loadUser(){
        console.log(this)
        this.setState({
            user: undefined,
            error: undefined
        })
        const userId = this.props.match.params['userId']
        userService.getById(userId)
            .then((data) => {
                this.setState({ user: data })
            })
            .catch((reason) => {
                this.setState({ error: reason })
            })
    }

    componentDidMount() {
        this.unlisten = this.props.history.listen(()=>{
            this.loadUser()
        })
        this.loadUser()
    }

    componentWillUnmount() {
        this.unlisten()
    }

    render() {
        const user = this.state.user;
        return (
            <div>
                <span>Usuario</span>
                <span>{this.props.match.params.userId}</span>
                <p>
                    { user ?
                        <span>
                            <span>name: </span>
                            <span>{user.name} </span>
                        </span>
                    :
                        <span>cargando</span>
                    }
                </p>
            </div>
        )
    }
}

const RoutedUser = compose(
    withRouter,
)(UserSketch);

const mapStateToProps = (state) => {
    return state.user;
};

const  mapDispatchToProps = (dispatch) => {
    return {
        dispatch
    };
};

const User = connect(mapStateToProps, mapDispatchToProps)(RoutedUser);

export { User, userChange };