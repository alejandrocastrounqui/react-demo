export const USER_CHANGE = 'USER_CHANGE'

export function userChange(user) {
    return {
        type: USER_CHANGE,
        user
    }
}