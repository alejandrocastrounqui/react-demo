import { isFalsy } from 'utility-types';

const defaultUsers = {
    "1": {
        name: 'Sergio'
    },
    "4":{
        name: 'Marco'
    },
    "8": {
        name: 'Joaquin'
    }
};

const usersRaw = localStorage.getItem(`usersMap`);
let users;

if(isFalsy(usersRaw)){
    localStorage.setItem(`usersMap`, JSON.stringify(defaultUsers));
    users = defaultUsers;
}
else{
    users = JSON.parse(usersRaw);
}

const delayedResolve = (result, time)=> {
    return new Promise((resolve)=> {
        setTimeout(()=>resolve(result), time);
    })
};

const delayedReject = (reason, time)=> {
    return new Promise((resolve, reject)=> {
        setTimeout(()=>reject(reason), time);
    })
};

const userService = {
    all(){
        return delayedResolve(users, 600);
    },
    getById(id){
        const user = users[id];
        if(!isFalsy(user)){
            return delayedResolve(user, 600);
        }
        return delayedReject("not found", 600);
    }
};

export { userService }