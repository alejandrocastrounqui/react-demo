import { USERS_LOADED, LOADING_USERS } from "../routes/users/actions";
import { USER_CHANGE } from "../routes/user/actions";

function usersDataReducer(state={users: null}, action){
    switch(action.type) {
        case LOADING_USERS:
            return {
                loading: true,
                users: null
            };
        case USERS_LOADED:
            console.log(action)
            return {
                loading: false,
                users: action.users
            };
        case USER_CHANGE:
            return { ...state,
                users: {
                    ...state.users,
                    [action.id]: action.user
                }
            };
        default:
            return state;
    }
}

export default usersDataReducer;