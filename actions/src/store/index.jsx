
import { createStore, combineReducers, applyMiddleware } from "redux";
import userDataReducer from "./user";
import usersDataReducer from "./users";
import thunk from 'redux-thunk';

const rootReducer = combineReducers({
    users: usersDataReducer,
    user: userDataReducer
})

export const store = createStore(rootReducer, applyMiddleware(thunk));