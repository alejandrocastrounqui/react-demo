import React from 'react';
import './App.css';
import { HashRouter, Link, Switch, Route } from 'react-router-dom';

import Home from "./routes/home";
import { Users } from "./routes/users";
import About from "./routes/about";

function App() {
  return (
      <div>
        <HashRouter>
            <nav>
              <ul>
                <li>
                  <Link to="/">Home</Link>
                </li>
                <li>
                  <Link to="/about">About</Link>
                </li>
                <li>
                  <Link to="/users">Users</Link>
                </li>
              </ul>
            </nav>

            {/* A <Switch> looks through its children <Route>s and
              renders the first one that matches the current URL. */}
            <Switch>
              <Route path="/about">
                <About />
              </Route>
              <Route path="/users">
                <Users />
              </Route>
              <Route path="/">
                <Home />
              </Route>
            </Switch>

        </HashRouter>
      </div>
  );
}

export default App;
