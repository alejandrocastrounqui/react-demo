import React from 'react';
import { HashRouter, Link, Switch, Route, withRouter } from 'react-router-dom';
import { compose } from 'recompose';
import User from "./user";

class Users extends React.Component {
    render() {
        let users = [{
            id: 1,
            name: 'Sergio'
        }, {
            id: 4,
            name: 'Marco'
        }, {
            id: 8,
            name: 'Joaquin'
        }]
        return (
            <div>
                <HashRouter>
                    <Switch>
                        <Route path={`${this.props.match.path}`} strict={true} exact={true}>
                            <p>
                                <span>Users</span>
                            </p>
                            <ul>
                                {users.map(({name, id}) => (
                                    <li key={id}>
                                        <Link to={`${this.props.match.path}/${id}`}>{name}</Link>
                                    </li>
                                ))}
                            </ul>
                        </Route>
                        <Route path={`${this.props.match.path}/:userId`} component={User}/>
                    </Switch>
                </HashRouter>
            </div>
        )
    }
}

export default compose(
    withRouter,
)(Users);