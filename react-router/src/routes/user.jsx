import React from 'react';
import { compose } from "recompose";
import { withRouter } from 'react-router-dom';

class User extends React.Component {
    render() {
        return (
            <div>
                <p>
                    <span>user</span>
                    <span> </span>
                    <span>{this.props.match.params.userId}</span>
                </p>
            </div>
        )
    }
}

export default compose(
    withRouter,
)(User);