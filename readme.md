<img src="assets/logo192.png" style="float: left; margin-right: 10px;"/>


# React demo

### ¿Que es react?    

- React es una libreria Javascript

[Lista de frameworks/librerias javascript]

Cubre 2 aspectos importantes de la programacion de paginas web

- Manipulacion DOM
- Asincronismo


### Manipulacion DOM

Document Object Model es una representacion logica de la pagina web

Si evaluamos la siguiente expresion podremos inspeccionarlo

```javascript
[window.document.body]
```

Es un arbol general (lista de nodos hijos) en el que cada nodo representa un elemento de la aplicacion y metodos que permiten interactuar con el mismo.

```
{ //body#gsr.srp.tbo.vasq.peek-rhs.BbLFkb
  aLink: "",
  accessKey: "",
  assignedSlot: null,
  attributeStyleMap: {
    StylePropertyMap []
  },
  attributes: { //NamedNodeMap
    0: {/*[...]*/} //jsmodel, 
    1: {/*[...]*/} //class, 
    2: {/*[...]*/} //jscontroller, 
    3: {/*[...]*/} //marginheight
    //[...]
  },
  autocapitalize: "",
  background: "",
  baseURI: "https://domain.com/path",
  bgColor: "",
  childElementCount: 23,
  childNodes:{ //NodeList(28)
    0: {/*[...]*/} //div#cst
    1: {/*[...]*/} //noscript
    2: {/*[...]*/} //div
    3: {/*[...]*/} //noscript
    4: {/*[...]*/} //style
    5: {/*[...]*/} //text
    6: {/*[...]*/} //div#searchform.jsrp
    7: {/*[...]*/} //div.sfbgx
    8: {/*[...]*/} //script
    //[...]
  }
  //[...]
}
```

El siguiente comando remplza toda la pagina por le texto "hola mundo"

```javascript
document.body.innerHTML = "hola mundo"
```

Asi mismo podemos construir nuestros propios elementos

```javascript
let elem = document.createElement('div');
let text = document.createTextNode('Hola mundo');
elem.appendChild(text);
document.body.appendChild(elem);
elem.style.position = 'absolute';
elem.style.backgroundColor = '#fa4';
```

React integra web components y un administrado llamado `React-dom` para procesar el arbol **DOM** dinamicamente.

```jsx
import React from 'react';

let functional = () =>{
  return <div></div>
}
```

Podemos ver los resultados en https://babeljs.io/repl

```javascript
functional = function functional() {
  return _react.default.createElement("div", null);
};
```

DOM funciona ademas como una red de eventos. Se puede emitir eventos hacia la raiz o hacia las hojas. Los componentes logicos asocian comportamiento a estos eventos. 

- Positivo: 
  - No se puede generar leaks. Cuando un elemento es removido del arbol todas las referencias pueden ser borradas
- Negativo: 
  - El procesamiento es ineficiente, cada nodo esta implicado.
  - Se pueden generar bucles infinitos 

Reac logra el mismo resultado mediante **actions**, con un unico punto de entrada, y subcripcion automatica

### Asincronismo

El concepto de asincronismo de asocia inmediatamente a AJAX pero en realidad la mayoria de las operaciones Javascript
son asincronicas. Leer el contenido de un documento adjunto, manejar un evento de teclado o mouse (no sabemos cuando sucede)

Como ejemplo AJAX es el mas importante

### ¿que es AJAX?

Asynchronous Javascript and XML

A mediados de los 90 los exploradores integran un cliente HTTP

- ActiveX XMLHTTP
- XMLHttpRequest

```javascript
if (window.XMLHttpRequest) {
    // code for modern browsers
    xmlhttp = new XMLHttpRequest();
 } else {
    // code for old IE browsers
    xmlhttp = new ActiveXObject("Microsoft.XMLHTTP");
}
```

El cliente nos permite obtener recursos de un servidor remoto

```javascript
var xhr = new XMLHttpRequest();
xhr.onreadystatechange = function() {
    if (xhr.readyState === 4){
        document.body.innerHTML = xhr.responseText;
    }
};
xhr.open('GET', 'http://ip.jsontest.com/');
xhr.send();
```

Pero exige ciertos recaudos

```javascript
var result = "vacio"
var xhr = new XMLHttpRequest();
xhr.onreadystatechange = function() {
    if (xhr.readyState === 4){
        result = xhr.responseText;
    }
};
xhr.open('GET', 'http://ip.jsontest.com/');
xhr.send();

document.body.innerHTML = result;
```

El flujo de ejecucion de este programa no es secuencial.

En 2006 JQuery lanza su primera version con la implementacion de ciertos mecanismos que hacian esta tarea mas intuitiva


```javascript
var script = document.createElement('script');
script.src = 'https://ajax.googleapis.com/ajax/libs/jquery/1.7.1/jquery.min.js';
script.type = 'text/javascript';
document.getElementsByTagName('head')[0].appendChild(script);
```

```javascript
jQuery.get("http://ip.jsontest.com", function(res){
  console.log(res)
})
```

Pero aun es complicado encadenar varios comportamientos asicronicos.

```javascript
jQuery.get("http://ip.jsontest.com", function(res){
  var srcList = [{
    name:"date"
  },{
    name:"headers"
  }]
  for(src of srcList){
    jQuery.get("http://"+ src.name +".jsontest.com", function(result){
      //esto no realiza la tarea que esperamos
      src.data = result;
    })
  }
  srcList.push({
    name: "ip",
    data: res
  })
  console.log("terminado?")
  //¿Como exponemos los resultados?
  //¿y si hay un error?
  window.srcList = srcList;
})
```

Este problema lo resuelven las [*promises*](https://developer.mozilla.org/es/docs/Web/JavaScript/Referencia/Objetos_globales/Promise).    
Las promises son un contenedor que puede tener un valor, podria tenerlo en el futuro o podría no tenerlo nunca (condicion de error). Estos contenedores son encadenables sobre su exito o error.


```javascript
var resultPromise = fetch("http://ip.jsontest.com")
.then((res)=>{
  var result = [{
    name: "ip",
    data: res
  }]
  var srcList = [{
    name:"date"
  },{
    name:"no-existo"
  },{
    name:"headers"
  }]
  // let crea un nuevo contexto
  for(let src of srcList){
    var promise = fetch("http://"+ src.name +".jsontest.com")
    .then((result)=>{
      src.data = result;
      return src;
    })
    .catch((reason)=>{
      //podemos propagar el error con 'throw reason'
      src.error = reason;
      return src;
    })
    result.push(promise);
  }
  return Promise.all(result);
})
```

Esta implementacion se basa en el estandar [Promise A+](https://promisesaplus.com/).    
El valor de retorno de un callback de exito es encadenado si respeta la interfaz **Thenable**

En el siguiente fragmento de la implmentacion de [D](https://github.com/malko/D.js/blob/master/lib/D.js) se puede observar un mecanismo de encadenamiento automatico.

```javascript
  //[...]
  try {
    var then = isObjOrFunc(val) && val.then;
    if ( isFunc(then) ) { // managing a promise
      if( val === _promise ){
        throw new tErr("Promise can't resolve itself");
      }
      then.call(val, once(_resolve), once(_reject));
      return this;
    }
  } catch (e) {
    once(_reject)(e);
    return this;
  }
  //[...]
```

### Setup

Se recomienda usar [Nave](https://github.com/isaacs/nave) para administrar el entorno Node

> $ exec nave use 10


> $ npx create-react-app reac-demo