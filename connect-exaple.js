

var dispatchNativo = function(action){
  console.log("dispatching", action);
};

var wrapper = function(actionConstructors){
  var result = {};
  var mkClosure = function(actionConstructor){
    return function(){
      var completeAction = actionConstructor.apply(null, arguments);
      dispatchNativo(completeAction);
    };
  };
  for(var actionConstructorName in actionConstructors){
    var actionConstructor = actionConstructors[actionConstructorName];
    result[actionConstructorName] = mkClosure(actionConstructor);
  }
  return result;
};

var action1Contrusctor = function(age){
  return {
    name: "miFirstAction",
    age: age
  };
};

var props = wrapper({
  action1: action1Contrusctor
});

props.action1();