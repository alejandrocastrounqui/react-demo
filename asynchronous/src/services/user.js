import { isFalsy } from 'utility-types';

const defaultUsers = [{
    id: 1,
    name: 'Sergio'
}, {
    id: 4,
    name: 'Marco'
}, {
    id: 8,
    name: 'Joaquin'
}];

const usersRaw = localStorage.getItem(`users`);
let users;

if(isFalsy(usersRaw)){
    localStorage.setItem(`users`, JSON.stringify(defaultUsers));
    users = defaultUsers;
}
else{
    users = JSON.parse(usersRaw);
}

const delayedResolve = (result, time)=> {
    return new Promise((resolve)=> {
        setTimeout(()=>resolve(result), time);
    })
};

const delayedReject = (reason, time)=> {
    return new Promise((resolve, reject)=> {
        setTimeout(()=>reject(reason), time);
    })
};

const userService = {
    all(){
        return delayedResolve(users, 600);
    },
    getById(id){
        for(let user of users){
            if(String(id) === String(user.id)){
                return delayedResolve(user, 600);
            }
        }
        return delayedReject("not found", 600);
    }
};

export { userService }