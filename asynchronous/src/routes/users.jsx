import React from 'react';
import PropTypes from "prop-types";
import { HashRouter, Link, Switch, Route, withRouter } from 'react-router-dom';
import { compose } from 'recompose';
import User from "./user";
import {userService} from "../services/user";

class Users extends React.Component {

    static propTypes = {
        match: PropTypes.object.isRequired,
        location: PropTypes.object.isRequired,
        history: PropTypes.object.isRequired
    };

    state = {
        user: null
    }

    componentDidMount() {
        userService.all()
        .then((data) => {
            console.log(data)
            this.setState({ users: data })
        })
        .catch(console.log)
    }

    render() {
        let users = this.state.users;
        return (
            <div>
                {this.state.users ?
                <HashRouter>
                    <Switch>
                        <Route path={`${this.props.match.path}`} strict={true} exact={true}>
                            <p>
                                <span>Users</span>
                            </p>
                            <ul>
                                {users.map(({name, id}) => (
                                    <li key={id}>
                                        <Link to={`${this.props.match.path}/${id}`}>{name}</Link>
                                    </li>
                                ))}
                            </ul>
                        </Route>
                        <Route path={`${this.props.match.path}/:userId`} component={User}/>
                    </Switch>
                </HashRouter>
                :
                <span>cargando</span>
                }
            </div>
        )
    }
}

export default compose(
    withRouter,
)(Users);